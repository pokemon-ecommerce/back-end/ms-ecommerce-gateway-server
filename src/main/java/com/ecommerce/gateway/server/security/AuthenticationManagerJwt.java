package com.ecommerce.gateway.server.security;

import com.ecommerce.gateway.server.mappers.JwtMapper;
import com.ecommerce.gateway.server.model.RealmAccess;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Collection;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

/**
 * AuthenticationManagerJwt .
 *
 * @author Carlos
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class AuthenticationManagerJwt implements ReactiveAuthenticationManager {

  private final ObjectMapper objectMapper;

  @Value("${jwt.publicKey}")
  private String publicKey;

  @Override
  public Mono<Authentication> authenticate(final Authentication authentication) {
    return Mono.just(authentication.getCredentials().toString())
        .map(this::validateToken)
        .map(this::claimToAuthentication);
  }

  private Authentication claimToAuthentication(Claims claims) {
    final JsonNode node = objectMapper.convertValue(claims.get("realm_access"), JsonNode.class);
    final RealmAccess realmAccess = JwtMapper.convertToObject(RealmAccess.class, node.toString());

    Collection<GrantedAuthority> authorities = realmAccess.getRoles().stream()
        .map(SimpleGrantedAuthority::new)
        .collect(Collectors.toList());

    return new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
  }

  private Claims validateToken(final String token) {
    return Jwts.parserBuilder().setSigningKey(getParsedPublicKey(publicKey)).build()
        .parseClaimsJws(token).getBody();
  }

  /**
   * getParsedPublicKey .
   *
   * @param key .
   * @return
   */
  private RSAPublicKey getParsedPublicKey(String key) {
    try {
      final byte[] decode = Base64.decode(key);
      final X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(decode);
      final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      return (RSAPublicKey) keyFactory.generatePublic(keySpecX509);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      log.error("error generate key", e.getLocalizedMessage());
      return null;
    }
  }

}
