package com.ecommerce.gateway.server.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * JwtMapper .
 *
 * @author Carlos
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class JwtMapper {

  /**
   * convertToObject .
   *
   * @param clazz      .
   * @param jsonString .
   * @param <T>        .
   * @return T
   */
  public static <T> T convertToObject(Class<T> clazz, String jsonString) {
    final var mapper = new ObjectMapper();
    try {
      return mapper.readValue(jsonString, clazz);
    } catch (Exception exception) {
      log.error("error json parser", exception);
      return null;
    }
  }

}
