package com.ecommerce.gateway.server.config;

import com.ecommerce.gateway.server.filters.JwtAuthenticationFilter;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * SecurityConfig .
 *
 * @author Carlos
 */
@EnableWebFluxSecurity
@RequiredArgsConstructor
public class SecurityConfig {

  private final JwtAuthenticationFilter authenticationFilter;

  /**
   * configure .
   *
   * @param http .
   * @return
   */
  @Bean
  public SecurityWebFilterChain configure(final ServerHttpSecurity http) {
    return http.authorizeExchange()
        .pathMatchers("/ms-ecommerce-auth-server/api/auth/login").permitAll()
        .pathMatchers("/ms-ecommerce-auth-server/api/auth/user").hasRole("USER")
        .anyExchange().authenticated()
        .and().addFilterAt(authenticationFilter, SecurityWebFiltersOrder.AUTHENTICATION)
        .csrf().disable()
        .build();
  }

}
