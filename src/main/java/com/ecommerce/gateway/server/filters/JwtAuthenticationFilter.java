package com.ecommerce.gateway.server.filters;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import reactor.core.publisher.Mono;

/**
 * JwtAuthenticationFilter .
 *
 * @author Carlos
 */
@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter implements WebFilter {

  private static final String PREFIX = "Bearer";

  private final ReactiveAuthenticationManager authenticationManager;

  @Override
  public Mono<Void> filter(final ServerWebExchange exchange, final WebFilterChain chain) {
    return Mono.justOrEmpty(exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION))
        .filter(authHeader -> authHeader.startsWith(PREFIX))
        .switchIfEmpty(chain.filter(exchange).then(Mono.empty()))
        .map(token -> token.replace(PREFIX, "").trim())
        .flatMap(token -> authenticationManager.authenticate(getAuthentication(token)))
        .flatMap(authentication -> chain.filter(exchange)
            .contextWrite(ReactiveSecurityContextHolder.withAuthentication(authentication)));
  }

  private Authentication getAuthentication(final String token) {
    return new UsernamePasswordAuthenticationToken(null, token);
  }

}
